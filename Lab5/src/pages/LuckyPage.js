import LuckyNumber from "../components/LuckyNumber";
import { useState } from "react";

function LuckyPage (){

    const [number, setnumber] =useState("");
    const [NumResult, setNumResult] =useState("0");
    
    function LuckyCal(){
        let num = parseInt(number);
        
        setNumResult(num);
        if (num == 69){
            setNumResult("ถูกแล้วจ้า");
        }else{
            setNumResult("ผิดจ้า!!");
        }
         }
    return(
              <div align="center"
              id="ResultBG">
            
                <h1>ยินดีต้อนรับสู่หน้าเพจสุ่มเลข</h1>
        
                <hr/>
                กรุณาทายตัวเลขที่ต้องการระหว่าง 0-99 : <input type="text"
                    value={number}
                    onChange= {(e) => {setnumber(e.target.value);}}/> <br/>
        <br/>
        <button onClick={ ()=>{ LuckyCal() } }> ทาย </button>
        {   NumResult != 0 && 
          <div>
        <hr/>
           นี่ผลการคำนวณจ้า
              
            <LuckyNumber
            number = {number}
            NumResult ={NumResult}
            />
        </div>
        }
                
             </div>
        
        
        
        
            );

}
export default LuckyPage;
    