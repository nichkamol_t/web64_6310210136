const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/bmi', (req, res) => {
    
    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}

    if ( !isNaN(weight) && !isNaN(height)){
        let bmi = weight / (height * height)
        result = {
            "status" : 200,
            "bmi" : bmi

        }
    }else {

        result = {
            "status" : 400,
            "message" : "Weight or Height is not a number"
        }
    }

    res.send(JSON.stringify(result))

  })

app.post('/hello', (req, res) => {
    res.send('Sawasdee ' + req.query.name)
  })

app.get('/triangle', (req, res) => {
    
    let height = parseFloat(req.query.height)
    let base = parseFloat(req.query.base)
    var result = {}

    if ( !isNaN(height) && !isNaN(base)){
        let area = (1/2) * base * height
        result = {
            "status" : 200,
            "area" : area

        }
    }else {

        result = {
            "status" : 400,
            "message" : "Height or Base is not a number"
        }
    }

    res.send(JSON.stringify(result))
  })


app.post('/score', (req, res) => {
    let name = 'Nichkamol'
    let score = parseFloat(req.query.score)
    var result = {}

    if ( !isNaN(score)){
        if (score >= 80 && score <= 100){
            res.send(name + ' get '+ score + ' Grade is A')
        }else if (score >= 75 && score < 80){
            res.send(name + ' get '+ score + ' Grade is B+')
        }else if (score >= 70 && score < 75){
            res.send(name + ' get '+ score + ' Grade is B')
        }else if (score >= 65 && score < 70){
            res.send(name + ' get '+ score + ' Grade is C+')
        }else if (score >= 60 && score < 65){
            res.send(name + ' get '+ score + ' Grade is C')
        }else if (score >= 55 && score < 60){
            res.send(name + ' get '+ score + ' Grade is D+')
        }else if (score >= 50 && score < 55){
            res.send(name + ' get '+ score + ' Grade is B')
        }else if (score >= 0 && score < 50){
            res.send(name + ' get '+ score + ' Grade is E')
        }else{
            res.send('Cannot be calculated')
        }   
    }else {

        result = {
            "status" : 400,
            "message" : "score is not a number"
        }
    }

    res.send(JSON.stringify(result))
  })
  

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})