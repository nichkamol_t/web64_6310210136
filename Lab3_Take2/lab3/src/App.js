import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Conj from './components/Conj';

function App() {
  return (
    <div className="App">
      <Header />
     <Body />
     <Conj />
     <Footer />
     
    </div>
  );
}

export default App;
